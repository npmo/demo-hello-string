# @demo/hello-string-bb

> Simple module to build a Hello User string

Here's a simple little demo module that builds a `Hello <User>` string.

This module has no dependencies.

# Install

```
npm install --save @demo/hello-string-bb
```

# Usage

```js
var helloString = require('@demo/hello-string-bb')
console.log(helloString('Bob'))
//=> Hello Bob
```
